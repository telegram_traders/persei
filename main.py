import os
from aiohttp import web
from app import create_app
import uvloop

uvloop.install()
application = create_app()


if __name__ == "__main__":
    web.run_app(application, host='0.0.0.0', port=int(os.getenv('PORT', 5001)))
