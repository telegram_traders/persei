from aiohttp import web
from app.view.exmo import registration, register2, unregistration, rabbit
from app.view.token import get_token


def setup_routes(app: web.Application):
    app.router.add_routes(
        [
            web.post('/exmo/registration', registration),
            web.post('/exmo/registration2', register2),
            web.delete('/exmo/', unregistration),
            web.get('/token', get_token),
            web.get('/rabbit', rabbit)
        ]
    )
