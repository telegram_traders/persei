from .tokenizer import Tokenizer
from aiohttp import web

TOKEN = 'Auth'


def authorization(func):
    async def wrapper(*args, **kwargs):
        request = args[0]
        pool = request.app['pool']
        token = request.headers.get(TOKEN)

        if not token:
            return web.Response(status=401, text='Token not found!')

        tokenizer = Tokenizer(pool)
        user = await tokenizer.get_user_by_token(token)
        if not user:
            return web.Response(status=404, text='User not found!')

        if not await tokenizer.check_token(user):
            return web.Response(status=401, text='Unauthorized')

        if 'data' not in request:
            request['data'] = {}

        request['data']['user'] = user
        return await func(*args, **kwargs)

    return wrapper
