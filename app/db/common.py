from . import Users


async def get_user(pool, name):
    async with pool.acquire() as conn:
        query = Users.select().where(Users.c.name == name)
        user = await conn.fetchrow(query)

        return user
