import asyncpgsa
from aiohttp import web
from config import (
    POSTGRES_HOST,
    POSTGRES_PORT,
    POSTGRES_USER,
    POSTGRES_PASS,
    POSTGRES_DATABASE,
)


async def init_postgres(app: web.Application):
    app['pool'] = await asyncpgsa.create_pool(
        host=POSTGRES_HOST,
        port=POSTGRES_PORT,
        database=POSTGRES_DATABASE,
        user=POSTGRES_USER,
        # loop=event_loop,
        password=POSTGRES_PASS,
        min_size=5,
        max_size=10
    )


async def close_postgres(app: web.Application):
    await app['pool'].close()
