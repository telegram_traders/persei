from os import environ as env
import asyncio
from aio_pika import Channel, connect_robust
from aio_pika.pool import Pool
from aiohttp import web


async def init_rabbit(app: web.Application):
    async def get_connection():
        return await connect_robust(env['RABBIT_HOST'])

    connection_pool = Pool(
        get_connection, max_size=int(env.get('RABBIT_CONNECTIONPOOLSIZE', 3)), loop=asyncio.get_event_loop()
    )
    app['rabbit_connection_pool'] = connection_pool

    async def get_channel() -> Channel:
        async with connection_pool.acquire() as connection:
            return await connection.channel()

    channel_pool = Pool(
        get_channel, max_size=int(env.get('RABBIT_CHANNELPOOLSIZE', 10)), loop=asyncio.get_event_loop()
    )
    app['rabbit_channel_pool'] = channel_pool


async def close_rabbit(app: web.Application):
    for connection in app['rabbit_connection_pool']._Pool__items._queue:
        await connection.close()
